package polinoame;

public class PRest {

	public Polinom cat;
	public Polinom rest;
	
	public PRest ()
	{
		cat=new Polinom();
		rest=new Polinom();
	}
	
	
	public void setRest(Polinom rest)
	{
		this.rest=rest;
	}
	
	
	public void setCat(Polinom cat)
	{
		this.cat=cat;
	}
}
