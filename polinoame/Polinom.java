package polinoame;

public class Polinom
{
	public static int GR_MAX=10;
	public float[] coeficient;
	public int grad;
		
	public Polinom ()
	{
		this.grad=0;
		this.coeficient=new float[GR_MAX+1];	
	}
	public Polinom(int grad)
	{
		this.grad=grad;
		this.coeficient=new float[grad+1];
	}
	public Polinom(int grad, float[] coef)
	{
		this.grad=grad;
		this.coeficient=coef;
	}

	public void setOneCoef(int index,float coef)
	{
			this.coeficient[index]=coef;
	}
	public int getGrMin(Polinom p)
	{
		if(this.grad>p.grad)
			return p.grad;
		else
			return this.grad;
	}
	public int getGrMax(Polinom p)
	{
		if(this.grad<p.grad)
			return p.grad;
		else
			return this.grad;
	}
	public Polinom getPolinomMax(Polinom p)
	{
		if(this.grad>p.grad)
			return this;
		else
			return p;
	}
		
	public Polinom adunare(Polinom p)
	{
		int max=this.getGrMax(p);
		int min=this.getGrMin(p);
		Polinom sum=new Polinom(max);	
		for(int i=0;i<=min;i++)
			sum.setOneCoef(i,this.coeficient[i]+p.coeficient[i]);
		if(this.grad!=p.grad)
		{
			Polinom g=this.getPolinomMax(p);
			for(int i=min+1;i<=max;i++)
				sum.setOneCoef(i,g.coeficient[i]);
		}
		return sum;	
	}
		
	public Polinom scadere(Polinom p)
	{
		int max=this.getGrMax(p);
		int min=this.getGrMin(p);
		Polinom dif=new Polinom(max);
		for(int i=0;i<=min;i++)
			dif.setOneCoef(i,this.coeficient[i]-p.coeficient[i]);
		if(this.grad<p.grad)
		{
			for(int i=min+1;i<=max;i++)
				dif.setOneCoef(i,-p.coeficient[i]);
		}
		else
		{
			if(this.grad>p.grad)
			{
				for(int i=min+1;i<=max;i++)
					dif.setOneCoef(i,this.coeficient[i]);
			}
			else 
			{
				while(dif.coeficient[min]==0 && dif.grad>0)
				{
					dif.grad--;
					min--;
				}
			}
		}
		return dif;	
	}
	public  Polinom inmultire(Polinom p)
	{
		Polinom rezultat=new Polinom(this.grad+p.grad);
		Polinom aux;
		for(int i=0;i<=this.grad;i++) 
		{
			for(int j=0;j<=p.grad;j++)
			{
				aux=new Polinom(i+j);
				aux.setOneCoef(i+j, this.coeficient[i]*p.coeficient[j]);
				rezultat=rezultat.adunare(aux);
			}
		}
		return rezultat;
	}
		
	public PRest impartire(Polinom p)
	{
			int i=this.grad-p.grad;
			int j=this.grad;
			int k=p.grad;
			PRest rezultat=new PRest();
			Polinom cat=new Polinom(i);
			Polinom catAux;
			Polinom rest=this;
			Polinom restAux;
			while(rest.grad>=p.grad)
			{
				catAux=new Polinom(i);
				catAux.setOneCoef(i, rest.coeficient[j]/p.coeficient[k]);
				restAux=p.inmultire(catAux);
				rest=rest.scadere(restAux);
				cat=cat.adunare(catAux);
				i=rest.grad-p.grad;
				j=rest.grad;
			}
			rezultat.setCat(cat);
			rezultat.setRest(rest);
			return rezultat;
	}
		
	public Polinom derivare()
	{
		Polinom rezultat=new Polinom(this.grad-1);
		for( int i=1;i<=this.grad;i++)
			rezultat.setOneCoef(i-1,i*this.coeficient[i]);
		return rezultat;
	}
		
	public Polinom integrare(float c)
	{
		Polinom rezultat=new Polinom(this.grad+1);
		rezultat.setOneCoef(0,c);
		for( int i=0;i<=this.grad;i++)
			rezultat.setOneCoef(i+1,this.coeficient[i]/(i+1));
		return rezultat;
	}
		
		
	public float valPol(float x)
	{
		float rezultat=0;
		for(int i=0;i<=this.grad;i++)
			rezultat+=Math.pow(x,i)*this.coeficient[i];
		return rezultat;
	}
		
	public Polinom radacini()
	{
		Polinom p=new Polinom(this.grad);
		int count=-1;
		float a=Math.abs(this.coeficient[0]);
		Polinom p1=this;
		Polinom p2=this;
		PRest polinomRest=new PRest();
		boolean ok=false;
		if(a==0)
		{
			ok=true;
			int k=0;
			while(this.coeficient[k]==0 && k<=this.grad)
				k++;
			Polinom polinom=new Polinom(k);
			polinom.setOneCoef(k,1);
			polinomRest=this.impartire(polinom);
			a=Math.abs(polinomRest.cat.coeficient[0]);
			while(k>0)
			{
				count++;
				p.setOneCoef(count,0);
				k--;
			}
		}
			
		for(int i=1;i<=a;i++)
		{
			if(a%i==0)
			{
				if(ok==true)
				{
					p1=polinomRest.cat;
					p2=polinomRest.cat;
				}
				else
				{
					 p1=this;
					 p2=this;
				}
				while(p1.valPol(i)==0)
				{
					count++;
					p.setOneCoef(count,i);
					p1=p1.derivare();
				}
				while(p2.valPol(-i)==0)
				{
					count++;
					p.setOneCoef(count,-i);
					p2=p2.derivare();
				}
			}
		}
		p.grad=count;
		return p;
	}
		
	public String print()
	{
		String rezultat="";
		if(coeficient[0]<0)
			rezultat=rezultat+" - ";
		for(int i=0;i<=grad;i++)
		{
			if(coeficient[i]!=0)
			{
				int a=(int)coeficient[i];
				if(i==0)
				{
					if((float)a==coeficient[i])
						rezultat=rezultat+Math.abs(a);
					else
						rezultat=rezultat+Math.abs(coeficient[i]);
				}
				else
				{
					if(coeficient[i]==1||coeficient[i]==-1)
						rezultat=rezultat+"X^"+i;
					else
					{
						if((float)a==coeficient[i])
							rezultat=rezultat+Math.abs(a)+"*X^"+i;
						else
							rezultat=rezultat+Math.abs(coeficient[i])+"*X^"+i;
					}
				}
				if(i!=grad)
				{
					if(coeficient[i+1]>0)
							rezultat=rezultat+" + ";
					if(coeficient[i+1]<0)
						rezultat=rezultat+" - ";
				}
			}
			else
			{
				if(grad==0)
					rezultat=rezultat+"0";
				else
				{
					if(i!=grad)
					{
						if(coeficient[i+1]<0)
							rezultat=rezultat+" - ";
						if(coeficient[i+1]>0)
							rezultat=rezultat+" + ";
					}
				}
			}
		}			
		return rezultat;
	}

}
